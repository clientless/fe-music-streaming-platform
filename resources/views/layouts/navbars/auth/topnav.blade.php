<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl
        {{-- {{ str_contains(Request::url(), 'virtual-reality') == true ? ' mt-3 mx-3 bg-primary' : '' }}" --}}
    id="navbarBlur"
    data-scroll="false">

    @if (isset(session()->all()['userinfo']))
        @if (session()->all()['userinfo']['role'] == 'ADMIN')
            @if (session()->all()['page'] == 'Dashboard')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Dashboard'])
            @elseif (session()->all()['page'] == 'Profile')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Profile'])
            @elseif (session()->all()['page'] == 'User Management')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'User Management'])
            @elseif (session()->all()['page'] == 'Genre')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Genre'])
            @elseif (session()->all()['page'] == 'Form Edit Genre')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Form Edit Genre'])
            @elseif (session()->all()['page'] == 'Form Add Genre')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Form Add Genre'])
            @elseif (session()->all()['page'] == 'Artist Live')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Artist Live'])
            @elseif (session()->all()['page'] == 'Artist Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Artist Live Detail'])
            @elseif (session()->all()['page'] == 'Artist Waiting Live')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Artist Waiting Live'])
            @elseif (session()->all()['page'] == 'Artist Waiting Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Artist Waiting Live Detail'])
            @elseif (session()->all()['page'] == 'Album Live')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Album Live'])
            @elseif (session()->all()['page'] == 'Album Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Album Live Detail'])
            @elseif (session()->all()['page'] == 'Album Waiting Live')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Album Waiting Live'])
            @elseif (session()->all()['page'] == 'Album Waiting Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Album Waiting Live Detail'])
            @elseif (session()->all()['page'] == 'Song Live')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Song Live'])
            @elseif (session()->all()['page'] == 'Song Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Song Live Detail'])
            @elseif (session()->all()['page'] == 'Song Waiting Live Detail')
                @include('layouts.navbars.auth.admin.topnavAdmin', ['title' => 'Song Waiting Live Detail'])
            @endif
        @elseif(session()->all()['userinfo']['role'] == 'PIC')
            @include('layouts.navbars.auth.pic.topnavPIC', ['title' => 'Your Dashboard'])
        @elseif(session()->all()['userinfo']['role'] == 'USER')
            @include('layouts.navbars.auth.user.topnavUser', ['title' => 'Dashboard'])
        @endif
    @else
    @endif

</nav>
<!-- End Navbar -->
