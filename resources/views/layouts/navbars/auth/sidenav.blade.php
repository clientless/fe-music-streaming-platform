<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 "
    id="sidenav-main">
    <div class="sidenav-header d-flex align-items-center justify-content-center">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{ route('home') }}" target="_blank">
            <img src="{{asset('assets/img/ToRing-MSP.png')}}" class="navbar-brand-img h-100" alt="main_logo">
        </a>
        <div class="ms-1 font-weight-bold">To Ring MSP</div>
    </div>
    @if (isset(session()->all()['userinfo']))
        @if (session()->all()['userinfo']['role'] == 'ADMIN')
            @include('layouts.navbars.auth.admin.sidenavAdmin')
        @elseif(session()->all()['userinfo']['role'] == 'PIC')
            @include('layouts.navbars.auth.pic.sidenavPIC')
        @elseif(session()->all()['userinfo']['role'] == 'USER')
            @include('layouts.navbars.auth.user.topnavUser')
        @endif
    @else
    @endif

</aside>
