@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav')
    <div class="row mt-4 mx-4">
        @include('pages.admin.song.live.detail')
    </div>
@endsection
