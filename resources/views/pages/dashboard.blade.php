@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav')

    <div class="container-fluid py-4">
        @if ($roleUser == 'ADMIN')
            @include('pages.admin.dashboardAdmin')
        @elseif ($roleUser == 'PIC')
            @include('pages.pic.dashboardPic')
        @endif
        @include('layouts.footers.auth.footer')
    </div>
@endsection

@push('js')
    <script src="./assets/js/plugins/chartjs.min.js"></script>

    <script>
        // var albumData = [@json($totalAlbumWaitingApproval), @json($totalAlbumWaitingApprovalIsApproved), @json($totalAlbumWaitingApprovalIsRejected)];
        // var artistData = [@json($totalArtistWaitingApproval), @json($totalArtistWaitingApprovalIsApproved), @json($totalArtistWaitingApprovalIsRejected)];
        // var currentChartType = 'album'; // Default chart type
        // document.getElementById('namaMetriks').innerText = 'Approval Album';
        // document.getElementById('logoMetriks').innerHTML =
        //     '<i class="ni ni-sound-wave text-lg opacity-10 text-success"></i>';
        // document.getElementById('buttonMetriks').innerHTML =
        //     '<button class="btn btn-primary btn-sm ms-auto" onclick="changeChart(\'album\')">Album</button> <button class = "btn btn-outline-warning btn-sm ms-2" onclick = "changeChart(\'artist\')"> Artist </button>';


        // var ctx = document.getElementById('barChart').getContext('2d');
        // var myBarChart = new Chart(ctx, {
        //     type: 'bar',
        //     data: {
        //         labels: ['Total', 'Total Approved', 'Total Rejected'],
        //         datasets: [{
        //             label: 'Data',
        //             data: albumData, // Default data
        //             backgroundColor: ['rgba(0, 0, 255, 0.2)', 'rgba(0, 255, 0, 0.2)',
        //                 'rgba(255, 0, 0, 0.2)'
        //             ],
        //             borderColor: ['rgba(0, 0, 255, 1)', 'rgba(0, 255, 0, 1)', 'rgba(255, 0, 0, 1)'],
        //             borderWidth: 1
        //         }]
        //     },
        //     options: {
        //         scales: {
        //             y: {
        //                 beginAtZero: true
        //             }
        //         }
        //     }
        // });

        // function changeChart(chartType) {
        //     // Update currentChartType
        //     currentChartType = chartType;

        //     // Update chart data based on selected type
        //     if (chartType === 'album') {
        //         myBarChart.data.datasets[0].data = albumData;
        //         document.getElementById('namaMetriks').innerText = 'Approval Album';
        //         document.getElementById('logoMetriks').innerHTML =
        //             '<i class="ni ni-sound-wave text-lg opacity-10 text-success"></i>';
        //         document.getElementById('buttonMetriks').innerHTML =
        //             '<button class="btn btn-primary btn-sm ms-auto" onclick="changeChart(\'album\')">Album</button> <button class = "btn btn-outline-warning btn-sm ms-2" onclick = "changeChart(\'artist\')"> Artist </button>';
        //     } else if (chartType === 'artist') {
        //         myBarChart.data.datasets[0].data = artistData;
        //         document.getElementById('namaMetriks').innerText = 'Approval Artist';
        //         document.getElementById('logoMetriks').innerHTML =
        //             '<i class="ni ni-badge text-lg opacity-10 text-success"></i>';
        //         document.getElementById('buttonMetriks').innerHTML =
        //             '<button class="btn btn-outline-warning btn-sm ms-auto" onclick="changeChart(\'album\')">Album</button> <button class = "btn btn-primary btn-sm ms-2" onclick = "changeChart(\'artist\')"> Artist </button>';
        //     }

        //     // Update the chart
        //     myBarChart.update();
        // }
    </script>
@endpush
