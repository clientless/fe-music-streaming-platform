<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }

    .album-image {
        border-radius: 20px;
        /* Membuat sudut gambar melengkung */
        object-fit: cover;
        /* Memastikan gambar terisi dengan baik dalam bingkai */
        width: 200px;
        /* Sesuaikan lebar gambar sesuai kebutuhan */
        height: 200px;
        /* Sesuaikan tinggi gambar sesuai kebutuhan */
    }
</style>
<div class="col-12">
    <div class="card">
        {{-- <form role="form" method="POST" action={{ route('approvalAlbumWaitingLiveDetailRejected') }} enctype="multipart/form-data" --}}
        <form role="form" enctype="multipart/form-data" id="form-update-profile" style="display: block;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0"><i class="fas fa-arrow-left" onclick="backToDetailProfile()"></i>&nbsp;Detail
                        Album</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <div class="form-group">
                        <img src="data:image/jpeg;base64,{{ $imgToBase64 }}" alt="Album Image"
                            class="img-fluid album-image">
                    </div>
                </div>
                <p class="text-uppercase text-sm">Album Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">ID Album</label>
                            <input class="form-control" type="text" name="idAlbum"
                                value="{{ $id }}" id="idAlbum" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Album Name</label>
                            <input class="form-control" type="text" name="albumName"
                                value="{{ $dataBody['titleAlbum'] }}" id="albumName" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Others Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Year</label>
                            <input class="form-control" type="text" name="yearActive"
                                value="{{ $dataBody['releasedYear'] }}" id="yearActive" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Genre</label>
                            <input class="form-control" type="text" name="genreType"
                                value="{{ $dataBody['genre'] }}" id="genreType" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-lg-0 mb-4">
                        <div class="card ">
                            <div class="card-header pb-0 p-3">
                                <div class="d-flex justify-content-between">
                                    <h6 class="mb-2">Track List</h6>
                                    {{-- <a href="#">
                                        <h6 class="ms-2">View All <i class="ni ni-bold-right"></i></h6>
                                    </a> --}}
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center ">
                                    <tbody>
                                        @if (empty($dataBody['songs']))
                                            <tr>
                                                <td class="text-center">
                                                    No Data Yet
                                                </td>
                                            </tr>
                                        @else
                                            @foreach ($dataBody['songs'] as $listSong)
                                                <tr>
                                                    <td class="w-30">
                                                        <div class="d-flex px-2 py-1 align-items-center">
                                                            {{-- <div>
                                                                <b>{{ $listAlbumWaiting['order_no'] }}</b>
                                                            </div> --}}
                                                            <div class="ms-4">
                                                                <a href="#">
                                                                    <p class="text-xs font-weight-bold mb-0">Title:</p>
                                                                    <h6 class="text-sm mb-0">
                                                                        {{ $listSong['titleSong'] }}</h6>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Language:</p>
                                                            <h6 class="text-sm mb-0">
                                                                {{ $listSong['language'] }}</h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Duration:</p>
                                                            <h6 class="text-sm mb-0">
                                                                {{ $listSong['durationTime'] }}</h6>
                                                        </div>
                                                    </td>
                                                    <td class="w-30">
                                                        <div class="col text-center">
                                                            <p class="text-xs font-weight-bold mb-0">&nbsp;</p>
                                                            <span
                                                                class="btn btn-success btn-xs ms-auto" onclick="pageToDetailSongLive('{{$listSong['idSong']}}')">Detail</span>
                                                            {{-- <button class="btn btn-danger btn-xs ms-2">Reject</button> --}}
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="d-flex justify-content-end ms-auto">
                    <span class="btn btn-danger btn-sm ms-4" id="button-to-cancel"
                        style="display:block; margin-right:2px" data-bs-toggle="modal"
                        data-bs-target="#rejectModal">Reject</span>
                    <span class="btn btn-primary btn-sm" id="button-to-edit" style="display:block;margin-left:2px"
                        data-bs-toggle="modal" data-bs-target="#accModal">Approve</span>
                </div>

            </div>

        </form>
    </div>

</div>


<script>
    function backToHome() {
        window.location.href = '/';
    }

    function backToDetailProfile() {
        // window.location.href = '/album/waiting/live';
        window.history.back();


    }

    function pageToDetailSongLive(idSong){
        window.location.href = '/song/waiting/live/detail/' + idSong;
    }


    function rejectAlbum() {
        var reasonField = document.getElementById('reasonFieldReject').value;
        var rejectBy = @json(session()->all()['userinfo']['username']);
        var selectedIdAlbum = document.getElementById('idAlbum').value;
        // console.log(reasonField);
        // console.log(rejectBy);
        // console.log(selectedIdAlbum);
        // console.log(selectedUsername);
        $.ajax({
            url: '/album/waiting/live/detail/approval/rejected', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PUT',
            data: {
                // username: selectedUsername,
                statusReject: {
                    isRejected: "Rejected",
                    rejectedBy: rejectBy,
                    reasonRejected: reasonField
                },
                statusApprove: {
                    isApproved: "",
                    approvedBy: "",
                    reasonApproved: ""
                },
                selectedIdAlbum: selectedIdAlbum,
                // reasonField: reasonField,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#rejectModal').modal('hide');
                if (response.status) {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
                window.location.href = '/album/waiting/live';
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#rejectModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/album/waiting/live';


            }
        });
    }

    function activateAlbum() {
        var accBy = @json(session()->all()['userinfo']['username']);
        var selectedIdAlbum = document.getElementById('idAlbum').value;
        $.ajax({
            url: '/album/waiting/live/detail/approval/approved', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PUT',
            data: {
                // username: selectedUsername,
                statusReject: {
                    isRejected: "",
                    rejectedBy: "",
                    reasonRejected: ""
                },
                statusApprove: {
                    isApproved: "Approved All",
                    approvedBy: accBy,
                    reasonApproved: "Valid to Approve"
                },
                selectedIdAlbum: selectedIdAlbum,
                // reasonField: reasonField,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#accModal').modal('hide');
                if (response.status) {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
                console.log(response.msg);
                window.location.href = '/album/waiting/live';
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#accModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/album/waiting/live';


            }
        });
    }
</script>


<!-- Modal Approve -->
<div class="modal fade" id="accModal" tabindex="-1" aria-labelledby="accModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="accModalLabel">
                    Approve Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Apakah Anda yakin ingin menyetujui Album ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak Yakin</button>
                <button type="button" class="btn btn-primary" onclick="activateAlbum()">Yakin</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reject -->
<div class="modal fade" id="rejectModal" tabindex="-1" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectModalLabel">Reject Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Anda akan menolak album ini dengan alasan di bawah:
                <div>
                    <textarea class="form-control" style="min-width: 100%" id='reasonFieldReject' name="reasonFieldReject" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="rejectAlbum()">Save</button>
            </div>
        </div>
    </div>
</div>
