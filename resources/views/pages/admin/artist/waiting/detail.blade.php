<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }

    .artist-image {
        border-radius: 20px;
        /* Membuat sudut gambar melengkung */
        object-fit: cover;
        /* Memastikan gambar terisi dengan baik dalam bingkai */
        width: 200px;
        /* Sesuaikan lebar gambar sesuai kebutuhan */
        height: 200px;
        /* Sesuaikan tinggi gambar sesuai kebutuhan */
    }
</style>
<div class="col-12">
    <div class="card">
        {{-- <form role="form" method="POST" action={{ route('approvalArtistWaitingLiveDetailRejected') }} enctype="multipart/form-data" --}}
        <form role="form" enctype="multipart/form-data" id="form-update-profile" style="display: block;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0"><i class="fas fa-arrow-left" onclick="backToDetailProfile()"></i>&nbsp;Detail
                        Artist</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <div class="form-group">
                        <img src="data:image/jpeg;base64,{{ $imgToBase64 }}" alt="Artist Image"
                            class="img-fluid artist-image">
                    </div>
                </div>
                <p class="text-uppercase text-sm">Artist Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">ID Artist</label>
                            <input class="form-control" type="text" name="idArtist"
                                value="{{ $dataBody['idArtist'] }}" id="idArtist" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Artist Name</label>
                            <input class="form-control" type="text" name="artistName"
                                value="{{ $dataBody['artistName'] }}" id="artistName" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Others Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Year</label>
                            <input class="form-control" type="text" name="yearActive"
                                value="{{ $dataBody['yearActive'] }}" id="yearActive" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Genre</label>
                            <input class="form-control" type="text" name="genreType"
                                value="{{ $dataBody['genreType'] }}" id="genreType" readonly>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end ms-auto">
                    <span class="btn btn-danger btn-sm ms-4" id="button-to-cancel"
                        style="display:block; margin-right:2px" data-bs-toggle="modal"
                        data-bs-target="#rejectModal">Reject</span>
                    <span class="btn btn-primary btn-sm" id="button-to-edit" style="display:block;margin-left:2px"
                        data-bs-toggle="modal" data-bs-target="#accModal">Approve</span>
                </div>

            </div>

        </form>
    </div>

</div>


<script>
    function backToHome() {
        window.location.href = '/';
    }

    function backToDetailProfile() {
        // window.location.href = '/artist/waiting/live';
        window.history.back();


    }

    function rejectAkun() {
        var reasonField = document.getElementById('reasonFieldReject').value;
        var rejectBy = @json(session()->all()['userinfo']['username']);
        var selectedIdArtist = document.getElementById('idArtist').value;
        // console.log(reasonField);
        // console.log(rejectBy);
        // console.log(selectedIdArtist);
        // console.log(selectedUsername);
        $.ajax({
            url: '/artist/waiting/live/detail/approval/rejected', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PUT',
            data: {
                // username: selectedUsername,
                statusReject: {
                    isRejected: "Rejected",
                    rejectedBy: rejectBy,
                    reasonRejected: reasonField
                },
                statusApprove: {
                    isApproved: "",
                    approvedBy: "",
                    reasonApproved: ""
                },
                selectedIdArtist: selectedIdArtist,
                // reasonField: reasonField,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#rejectModal').modal('hide');
                if (response.status) {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
                window.location.href = '/artist/waiting/live';
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#rejectModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/artist/waiting/live';


            }
        });
    }

    function activateAkun() {
        var accBy = @json(session()->all()['userinfo']['username']);
        var selectedIdArtist = document.getElementById('idArtist').value;
        $.ajax({
            url: '/artist/waiting/live/detail/approval/approved', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PUT',
            data: {
                // username: selectedUsername,
                statusReject: {
                    isRejected: "",
                    rejectedBy: "",
                    reasonRejected: ""
                },
                statusApprove: {
                    isApproved: "Approved",
                    approvedBy: accBy,
                    reasonApproved: "Valid to Approve"
                },
                selectedIdArtist: selectedIdArtist,
                // reasonField: reasonField,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#accModal').modal('hide');
                if (response.status) {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
                console.log(response.msg);
                window.location.href = '/artist/waiting/live';
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#accModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/artist/waiting/live';


            }
        });
    }
</script>


<!-- Modal Approve -->
<div class="modal fade" id="accModal" tabindex="-1" aria-labelledby="accModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="accModalLabel">
                    Approve Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Apakah Anda yakin ingin menyetujui Akun ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak Yakin</button>
                <button type="button" class="btn btn-primary" onclick="activateAkun()">Yakin</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reject -->
<div class="modal fade" id="rejectModal" tabindex="-1" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectModalLabel">Reject Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Anda akan menolak akun ini dengan alasan di bawah:
                <div>
                    <textarea class="form-control" style="min-width: 100%" id='reasonFieldReject' name="reasonFieldReject" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="rejectAkun()">Save</button>
            </div>
        </div>
    </div>
</div>
