<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }

    .artist-image {
        border-radius: 20px;
        /* Membuat sudut gambar melengkung */
        object-fit: cover;
        /* Memastikan gambar terisi dengan baik dalam bingkai */
        width: 200px;
        /* Sesuaikan lebar gambar sesuai kebutuhan */
        height: 200px;
        /* Sesuaikan tinggi gambar sesuai kebutuhan */
    }
</style>
<div class="col-12">
    <div class="card">
        {{-- <form role="form" method="POST" action={{ route('approvalArtistWaitingLiveDetailRejected') }} enctype="multipart/form-data" --}}
        <form role="form" enctype="multipart/form-data" id="form-update-profile" style="display: block;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0"><i class="fas fa-arrow-left" onclick="backToDetailProfile()"></i>&nbsp;Detail
                        Artist</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <div class="form-group">
                        <img src="data:image/jpeg;base64,{{ $imgToBase64 }}" alt="Artist Image"
                            class="img-fluid artist-image">
                    </div>
                </div>
                <p class="text-uppercase text-sm">Artist Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">ID Artist</label>
                            <input class="form-control" type="text" name="idArtist"
                                value="{{ $dataBody['idArtist'] }}" id="idArtist" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Artist Name</label>
                            <input class="form-control" type="text" name="artistName"
                                value="{{ $dataBody['artistName'] }}" id="artistName" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Others Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Year</label>
                            <input class="form-control" type="text" name="yearActive"
                                value="{{ $dataBody['yearActive'] }}" id="yearActive" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Genre</label>
                            <input class="form-control" type="text" name="genreType"
                                value="{{ $dataBody['genreType'] }}" id="genreType" readonly>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end ms-auto">
                    <button type="button" class="btn btn-outline-warning btn-sm ms-4" id="button-to-back"
                        style="display:block; margin-right:2px" onclick="backToDetailProfile()">Back</button>
                </div>

            </div>

        </form>
    </div>

</div>


<script>
    function backToHome() {
        window.location.href = '/';
    }

    function backToDetailProfile() {
        window.location.href = '/artist/live';

    }

</script>
