<?php

namespace App\Listeners;

use App\Events\LogoutUser;
use Illuminate\Auth\Events\Logout;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class LogoutUserListener
{
    public function handle(LogoutUser $event)
    {
        Auth::logoutOtherDevices($event->userId);
    }
}
