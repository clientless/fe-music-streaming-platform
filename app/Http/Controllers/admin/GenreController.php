<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;


class GenreController extends Controller
{
    //
    public function showGenre()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Genre');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $listGenre = self::listGenre($auth);

        $message = $listGenre['message'];
        $statusCode = $listGenre['code'];
        $dataBody = $listGenre['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully get object');
        if ($status) {
            return view('pages.genre', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showFormEditGenre(Request $request, $idGenre)
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Form Edit Genre');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $codeGenre = $idGenre;
        $dataGenreByCode = self::dataGenreByCode($auth, $codeGenre);

        $message = $dataGenreByCode['message'];
        $statusCode = $dataGenreByCode['code'];
        $dataBody = $dataGenreByCode['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get data by code : ' . strtolower($codeGenre));
        if ($status) {
            return view('pages.formEditGenre', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function saveFormEditGenre(Request $request)
    {
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];

        $genereDataEdit = [
            'genreType' => $request->newGenreType,
            'updatedBy' => $request->updatedBy,
        ];

        $genreCode = $request->genreCode;


        try {
            // Make the API request
            $response = Http::withHeaders([
                'Authorization' => $auth,
                'Accept' => 'application/json',
            ])->put('http://127.0.0.1:8081/v1/genre/edit/' . $genreCode, $genereDataEdit);

            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();
            // dd($statusCode, $responseData);

            // Check the API response status code
            if ($statusCode == 200) {
                Log::info("Sukses Masuk");
                $dataResponseApiSuccess = $responseData['data'];
                // return redirect()->route('login')->withSuccess(['message' => $responseData['message']]);
                return redirect()->route('genre')->withSuccess(['message' => $responseData['message']]);
            } else if (!empty($responseData)) {
                return redirect()->route('genre')->withErrors(['message' => $responseData['message']]);
            } else {
                return redirect()->route('genre')->withErrors(['message' => 'Error occurred during update.']);
            }
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return redirect()->route('genre')->withErrors(['message' => 'Error occurred during update.']);
        }
    }

    public function saveDeleteGenre(Request $request)
    {
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];

        $genreCode = $request->genreCode;
        $reasonField = $request->reasonField;


        $detailDataUser = self::detailProfile($auth, $username);
        $messageDataUser = $detailDataUser['message'];
        $statusCodeDataUser = $detailDataUser['code'];
        $dataBodyDataUser = $detailDataUser['data'];
        $statusDataUser = ($statusCodeDataUser == 200 && strtolower($messageDataUser) == 'data found');
        $fullName = '';
        // $decrypt = Crypt::decrypt();


        if ($statusDataUser) {
            // return view('pages.user-profile', compact('dataBody'));
            $fullName .= $dataBodyDataUser['fullname'];
        } else {
            Log::error('Error process delete ' . $messageDataUser);
            return response()->json(['status' => $statusDataUser, 'msg' => $messageDataUser, 'data' => $dataBodyDataUser]);
        }


        $dataToDelete = [
            'deletedBy' => $fullName,
            'reasonDeleted' => $reasonField,
        ];


        try {
            // Make the API request

            $response = Http::withHeaders([
                'Authorization' => $auth,
            ])->asForm()->delete('http://127.0.0.1:8081/v1/genre/delete/by/' . $genreCode, $dataToDelete);


            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();
            $message = $responseData['message'];
            $statusCode = $responseData['code'];
            $dataBody = $responseData['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'successfully to deleted'); //Successfully to deleted
            // dd($statusCode, $responseData);

            // Check the API response status code
            if ($status) {
                Log::info("Sukses Masuk");
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else if (!empty($responseData)) {
                Log::error('Error process delete ' . $message);
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                Log::error('Error process delete ' . $message);
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage(), 'data' => null]);
        }
    }

    public function showFormGenre(Request $request)
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Form Add Genre');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // $codeGenre = $idGenre;
        $detailDataUser = self::detailProfile($auth, $username);
        $messageDataUser = $detailDataUser['message'];
        $statusCodeDataUser = $detailDataUser['code'];
        $dataBodyDataUser = $detailDataUser['data'];
        $statusDataUser = ($statusCodeDataUser == 200 && strtolower($messageDataUser) == 'data found');
        $fullName = '';
        $dateNow = date("Y-m-d");
        // $decrypt = Crypt::decrypt();


        if ($statusDataUser) {
            // return view('pages.user-profile', compact('dataBody'));
            $fullName .= $dataBodyDataUser['fullname'];
            return view('pages.formAddGenre', compact('fullName', 'dateNow'));
        } else {
            Log::error('Error process add ' . $messageDataUser);
            // return response()->json(['status' => $statusDataUser, 'msg' => $messageDataUser, 'data' => $dataBodyDataUser]);
            return redirect()->route('/')->withErrors(['message' => $messageDataUser]);
        }
    }

    public function saveGenre(Request $request)
    {
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];

        $genereData = [
            'genreType' => $request->genreType,
            'createdBy' => $request->createdBy,
        ];

        try {
            // Make the API request
            $response = Http::withHeaders([
                'Authorization' => $auth,
                'Accept' => 'application/json',
            ])->post('http://127.0.0.1:8081/v1/genre/upload', $genereData);

            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();
            // dd($statusCode, $responseData);

            // Check the API response status code
            if ($statusCode == 200) {
                Log::info("Sukses Masuk");
                $dataResponseApiSuccess = $responseData['data'];
                // return redirect()->route('login')->withSuccess(['message' => $responseData['message']]);
                return redirect()->route('genre')->withSuccess(['message' => $responseData['message']]);
            } else if (!empty($responseData)) {
                return redirect()->route('genre')->withErrors(['message' => $responseData['message']]);
            } else {
                return redirect()->route('genre')->withErrors(['message' => 'Error occurred during update.']);
            }
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return redirect()->route('genre')->withErrors(['message' => 'Error occurred during update.']);
        }
    }





    private static function listGenre($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/genre/list';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function dataGenreByCode($authorizationUser, $codeGenre)
    {
        $token = $authorizationUser;
        $code = $codeGenre;
        $url = 'http://127.0.0.1:8081/v1/genre/by/' . $code;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailProfile($authorizationUser, $username)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8082/v1/admin/' . $username;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $dataResponse = $response->json();

        return $dataResponse;
    }
}
