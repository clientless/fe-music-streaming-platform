<?php

namespace App\Http\Controllers\PDFController;

use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PDFController extends Controller
{
    public function downloadPDF(Request $request)
    {
        $dataBody = $request->dataBody;

        // Load view and pass data to it
        $pdf = PDF::loadView('test', ['dataBody' => $dataBody]);

        // Set response headers
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="itsolutionstuff.pdf"'
        ];

        // Return response with PDF content and headers
        return Response::make($pdf->output(), 200, $headers);
    }
}
