<?php

// app\Providers\CustomAuthServiceProvider.php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use App\Auth\CustomUserProvider;
use App\Models\CustomUser;

class CustomAuthServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AuthFactory::class, function ($app) {
            return new CustomUserProvider($app['hash'], new CustomUser('user@example.com', bcrypt('password')));
        });
    }
}
